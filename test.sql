/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-12-22 19:07:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'admin', '超级管理员');

-- ----------------------------
-- Table structure for `course`
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `Id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `teacher_course` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '面向对象程序设计', '201');
INSERT INTO `course` VALUES ('2', '软件项目管理', '202');
INSERT INTO `course` VALUES ('3', '基于ssh框架项目开发', '203');

-- ----------------------------
-- Table structure for `score`
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `score` double(6,1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `stu_score` (`student_id`),
  KEY `course_score` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES ('1', '101', '1', '100.0');
INSERT INTO `score` VALUES ('2', '102', '1', '99.0');
INSERT INTO `score` VALUES ('4', '101', '3', '222.0');

-- ----------------------------
-- Table structure for `student`
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `sex` int(20) DEFAULT NULL,
  `clazz` varchar(20) DEFAULT NULL,
  `birthday` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('101', '林大雷', '1111', '1', '13软件2', '1996-03-24');
INSERT INTO `student` VALUES ('102', '萧炎亮', '0000', '0', '13软件1', '1995-08-01');
INSERT INTO `student` VALUES ('103', '叶凡凯', '0000', '1', '13软件1', '1994-01-23');
INSERT INTO `student` VALUES ('104', '李牧尘', '1111', '0', '13软件1', '1997-12-05');
INSERT INTO `student` VALUES ('105', '刘红枫', '0000', '1', '13软件2', '1995-11-15');
INSERT INTO `student` VALUES ('106', '昭', '123', null, null, null);
INSERT INTO `student` VALUES ('107', '昭', '123', null, null, null);
INSERT INTO `student` VALUES ('108', '老罗', '123', null, null, null);
INSERT INTO `student` VALUES ('109', '李青山', '123', null, null, null);
INSERT INTO `student` VALUES ('110', '石文柱', '123', null, null, null);

-- ----------------------------
-- Table structure for `teacher`
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `Id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `birthday` varchar(20) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `professional` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `course_teacher` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
