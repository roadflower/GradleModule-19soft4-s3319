package servlet;

import dao.StudentDAOImpl;
import domain.Student;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.StudentServiceImpl;
import util.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/toUpdateStudentServlet")
public class ToUpdateStudentServlet extends HttpServlet
{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //创建引擎
        TemplateEngine engine= TemplateEngineUtil.getTemplateEngine(req.getServletContext());
        //构造应用的上下文环境
        WebContext context = new WebContext(req, resp, req.getServletContext());
        //相应页面设置字符集utf-8
        resp.setCharacterEncoding("utf-8");
        // 获取参数id的值
        int id=Integer.parseInt(req.getParameter("id"));
        //创建service层的对象
        StudentServiceImpl studentService=new StudentServiceImpl();
        //调用dao的getStudent方法，获取要更新的学生信息
        Student student=studentService.getStudent(id);
        //将学生对象设置成Attribute属性值，准备传递到updateStudent.html页面
        req.setAttribute("student",student);

       //引擎转向到updateStudent.html页面
        //只有引擎处理过的页面才会被解析th标签
        engine.process("updateStudent.html",context,resp.getWriter());
    }
}

