package servlet;

import domain.Student;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.StudentServiceImpl;
import util.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
@WebServlet("/UpdateStudentServlet")
public class UpdateStudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        int id=Integer.parseInt(req.getParameter("id"));
        String name=req.getParameter("name");
        String password=req.getParameter("password");
        int sex=Integer.parseInt(req.getParameter("sex"));
        String clazz=req.getParameter("clazz");
        String birthday=req.getParameter("birthday");


        TemplateEngine engine= TemplateEngineUtil.getTemplateEngine(req.getServletContext());
        WebContext context = new WebContext(req, resp, req.getServletContext());
        resp.setCharacterEncoding("utf-8");

        //deliver student object to showUsername.html
        //req.setAttribute("student",student);
        List students=new ArrayList();
        try {
            //获取业务层对象
            StudentServiceImpl studentService=new StudentServiceImpl();
            Student newStudent = new Student();
            newStudent.setId(id);
            newStudent.setName(name);
            newStudent.setPassword(password);
            newStudent.setSex(sex);
            newStudent.setClazz(clazz);
            newStudent.setBirthday(birthday);
            //调用业务层方法更新学生
            studentService.updateStudent(newStudent);

            //转到selectStudentsServlet
            resp.sendRedirect("selectStudentsServlet");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
