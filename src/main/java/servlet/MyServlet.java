package servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Index")
public class MyServlet extends HttpServlet {
    public MyServlet(){
        super();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("init");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("service");
    }

    @Override
    public void destroy() {
        System.out.println("destroy");
    }
//    public void doGet(HttpServletRequest request, HttpServletResponse response)
//        throws ServletException, IOException{
//        //相应的字符输出流，客户端的浏览器
//        PrintWriter out=response.getWriter();
//        //打印客户端的ip
//        out.println(request.getRemoteAddr());
//        //打印servlet的类名
//        out.println(this.getClass());
//        //刷新输出端的缓存
//        out.flush();
//        //关闭输出流
//        out.close();
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("post method");
//        doGet(req,resp);
//    }
}
