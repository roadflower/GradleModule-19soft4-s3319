package servlet;


import domain.Course;
import domain.Student;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import service.CourseServiceImpl;
import util.Pager;
import util.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
//定义用户在浏览器地址栏里如何访问该Servlet
@WebServlet("/SelectCoursesServlet")
public class SelectCoursesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        TemplateEngine engine= TemplateEngineUtil.getTemplateEngine(req.getServletContext());
        WebContext context = new WebContext(req, resp, req.getServletContext());
        resp.setCharacterEncoding("utf-8");

        //deliver student object to showUsername.html
        //req.setAttribute("student",student);
        //  List students=new ArrayList();

        try {
            //1. 获取“当前页”参数；  (第一次访问当前页为null)
            String currPage = req.getParameter("currentPage");
            // 判断
            if (currPage == null || "".equals(currPage.trim())){
                currPage = "1";      // 第一次访问，设置当前页为1;
            }
            // 转换
            int currentPage = Integer.parseInt(currPage);

            //2. 创建PageBean对象，设置当前页参数； 传入service方法参数
            Pager<Course> pageBean = new Pager<Course>();
            pageBean.setCurrentPage(currentPage);
//deliver Course object to showUsername.html
//req.setAttribute("Course",Course);
            //alt+enter
            CourseServiceImpl CourseService=new CourseServiceImpl();
            CourseService.getCourses(pageBean);





//deliver Courses to showUsername.html
            req.setAttribute("pageBean",pageBean);

            engine.process("listCourses.html",context,resp.getWriter());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

