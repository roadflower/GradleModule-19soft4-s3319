package servlet;

import domain.Student;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.StudentServiceImpl;
import util.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/DeleteStudentServlet")
public class DeleteStudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        TemplateEngine engine= TemplateEngineUtil.getTemplateEngine(req.getServletContext());
        WebContext context = new WebContext(req, resp, req.getServletContext());
        resp.setCharacterEncoding("utf-8");

        //deliver student object to showUsername.html
        //req.setAttribute("student",student);
        List students=new ArrayList();
        try {
            //获取业务层对象
            StudentServiceImpl studentService=new StudentServiceImpl();
           //获取传过来的id值
            int id=Integer.parseInt(req.getParameter("id"));
            //调用service层的删除学生方法
            studentService.deleteStudent(id);

            //转到selectStudentsServlet
            resp.sendRedirect("selectStudentsServlet");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

