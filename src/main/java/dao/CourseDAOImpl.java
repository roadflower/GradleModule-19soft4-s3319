package dao;

import domain.Course;
import domain.Student;
import util.DBUtil;
import util.Pager;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CourseDAOImpl {
    //获取记录总数
    public int getTotalCount() {
        String sql = "select count(*) as num from course";
        try {
            // 创建QueryRunner对象
            ResultSet rs = DBUtil.executeQuery(sql, null);
            int count=0;
            // 执行查询， 返回结果的第一行的第一列
            if(rs.next())
                count=rs.getInt("num");
            return count;

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }
    //获取course表数据
    public void getCourses(Pager<Course> pb){
        //2. 查询总记录数;  设置到pb对象中
        int totalCount = this.getTotalCount();
        pb.setTotalCount(totalCount);

        // 判断
        if (pb.getCurrentPage() <=0) {
            pb.setCurrentPage(1);                        // 把当前页设置为1
        } else if (pb.getCurrentPage() > pb.getTotalPage()){
            pb.setCurrentPage(pb.getTotalPage());        // 把当前页设置为最大页数
        }

        //1. 获取当前页： 计算查询的起始行、返回的行数
        int currentPage = pb.getCurrentPage();
        int index = (currentPage -1 ) * pb.getPageCount();        // 查询的起始行
        int count = pb.getPageCount();                            // 查询返回的行数


        //3. 分页查询数据;  把查询到的数据设置到pb对象中
        String sql = "select * from course limit ?,?";

        try {
            // 得到Queryrunner对象
            Object[] params={index,count};
            ResultSet rs = DBUtil.executeQuery(sql,params);
            // 根据当前页，查询当前页数据(一页数据)
            List pageData=new ArrayList();

            //循环显示rs对象中的数据;rs.next()遍历指针
            Course course;
            while(rs.next())
            {
                //每一条记录，转变成一个Student对象
                course=new Course();
                course.setId(rs.getInt("id"));
                course.setName(rs.getString("name"));
                course.setTeacher_id(rs.getInt("teacher_id"));

                //把course对象加入courses列表
                pageData.add(course);

            }
            // 设置到pb对象中
            pb.setPageData(pageData);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

}
