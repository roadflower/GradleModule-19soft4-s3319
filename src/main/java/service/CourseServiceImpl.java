package service;

import dao.CourseDAOImpl;
import domain.Course;
import util.Pager;

public class CourseServiceImpl {
    //创建DAO数据访问层对象
    CourseDAOImpl CourseDAO=new CourseDAOImpl();
    //list Courses
    public void getCourses(Pager<Course> pb){
//调用数据访问层对象的方法
        CourseDAO.getCourses(pb);
    }
}
