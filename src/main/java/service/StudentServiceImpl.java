package service;

import dao.StudentDAOImpl;
import domain.Student;
import util.Pager;

import javax.xml.transform.Result;
import java.sql.ResultSet;
//业务层
public class StudentServiceImpl {
    //创建DAO数据访问层对象
    StudentDAOImpl studentDAO=new StudentDAOImpl();
    //list students
    public void getStudents(Pager<Student> pb){
//调用数据访问层对象的方法
        studentDAO.getStudents(pb);
    }

      //delete student
    public boolean deleteStudent(int id){
        return studentDAO.deleteStudent(id);
    }
    //get student
    public Student getStudent(int id)
    {
        return studentDAO.getStudent(id);
    }

    //update student
    public boolean updateStudent(Student student){
       return studentDAO.updateStudent(student);
    }

    //add student
    public boolean addStudent(Student student){return studentDAO.addStudent(student);}
}
